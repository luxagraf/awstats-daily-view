# AWStats Daily View

JavaScript and CSS to add links for viewing AWStats reports by day.

### Adds links across the top of your AWStats page like this:

<img src="screenshot.png" />

A full tutorial is available at [TODO]()


### Requirements

* Working AWStats install

See my [AWStats tutorial](TODO) if you need help with that. 

### Quick Install Guide

First you need to make sure you're creating Daily reports in AWStats. The documentation on the syntax can be found in the [AWStats Docs](https://awstats.sourceforge.io/docs/awstats_faq.html#DAILY). 

In Ubuntu 18.04 and similar distros you can easily do this by editing your logrotate file. The only part I'll show here is the prerotate directive which will call our `databasebreak=day` command to build the daily reports.

`/etc/logrotate.d/nginx`:

    prerotate
        /usr/share/doc/awstats/examples/awstats_updateall.pl now -awstatsprog=/usr/lib/cgi-bin/awstats.pl
        /usr/lib/cgi-bin/awstats.pl -config=mydomain.com -update -databasebreak=day
        if [ -d /etc/logrotate.d/httpd-prerotate ]; then \
            run-parts /etc/logrotate.d/httpd-prerotate; \
        fi \
    endscript

Save that file. If you don't want to wait a day for logrotate to actually run that script you can run it now manually:

    /usr/lib/cgi-bin/awstats.pl -config=mydomain.com -update -databasebreak=day
    
Now you have the reports being built, you just need to add the files to your web server. 

Assuming you're serving your AWStats site out of `/var/www/awstats` you'd just:

    cd /var/www/awstats
    git clone https://gitlab.com/luxagraf/awstats-daily-view.git
    
Now just open up your awstats.mydomain.com.conf file and search for `HTMLHeadSection`. Change it to look like this:

    HTMLHeadSection="<script src='/awstats-daily-view/src/daily.js'></script><link rel='stylesheet' href='/awstats-daily-view/src/daily.css' media='screen'>"      

Reload your AWStats page and you should see the links like the top of this page.

If you have problems open an issue.