function daysInMonth (month, year) { // Use 1 for January, 2 for February, etc.
    return new Date(year, month, 0).getDate();
}
function createDailyMenu() {
    var d = new Date() ;
    var today = d.getUTCDate(); 
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    var days_in_month = daysInMonth(month, year)
    var url_base = window.location.pathname;
    var url_params = new URLSearchParams(window.location.search);
    var ul = document.createElement("ul");
    ul.className = "daily-wrapper";

    for (var i = 1; i < days_in_month; i++) {
        var li = document.createElement("li");
        var link = document.createElement("a");
        if (i < 10) {
            var day = "0"+i;
        } else {
            var day = i;
        }
        var url = url_base
        for(pair of url_params.entries()) { 
            if (pair[0] == 'config') {
                var params = "?config="+pair[1];
            } else if (pair[0] !== "framename" & pair[0] !== "day") {
                params += "&"+pair[0]+"="+pair[1];
            } 
        }
        if (!(url_params.has("databasebreak"))) {
            url_params.append("databasebreak", "day");
        }
        if (i <= today){
            link.href= url_base+params+"&day="+day;
            link.innerHTML = day;
            link.target = "_top";
            li.append(link);
        } else {
            li.innerHTML = day;    
            li.className = "no-link";
        }
        ul.append(li);
    }
    for (var i = 0; i < window.frames.length; i++) {
        console.log(window.frames[i]);
    }
    var return_link = document.createElement("a");
    return_link.href= window.location.pathname+"?config="+url_params.get("config");
    return_link.target = "_top";
    return_link.className = "return-link";
    return_link.innerHTML = "Return to Monthly View";
    ul.append(return_link);
    document.body.appendChild(ul);
}
createDailyMenu();
